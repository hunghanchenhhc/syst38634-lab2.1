package phones;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmartPhoneTest {

	@Test
	public void testGetFormattedPriceRegular() {
		SmartPhone sm = new SmartPhone("Apple", 1500.00, 2.00);
		assertTrue("Invalid format", "$1,500".equals(sm.getFormattedPrice()));
	}
	
	@Test( expected = ArithmeticException.class)
	public void testGetFormattedPriceException () {
		SmartPhone sm = new SmartPhone("Apple", 1/0 , 2.00);
		System.out.println(sm.getFormattedPrice());
		assertFalse("Invalid format", "$1,500".equals(sm.getFormattedPrice()));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryIn() {
		SmartPhone sm = new SmartPhone("Apple", 999.0, 2.00);
		assertTrue("Invalid format", "$999".equals(sm.getFormattedPrice()));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		SmartPhone sm = new SmartPhone("Apple", 1500.10, 2.00);
		assertFalse("Invalid format", "$1,500.10".equals(sm.getFormattedPrice()));
	}

	@Test
	public void testSetVersionRegular() {
		SmartPhone sm = new SmartPhone();
		try {
			sm.setVersion(2.00);
		} catch (VersionNumberException e) {
			System.out.println(e.getMessage());
		}	
		assertTrue("Invalid version",2.0==sm.getVersion() );
	}
	
	@Test(expected = VersionNumberException.class)
	public void testSetVersionException() throws VersionNumberException {
		SmartPhone sm = new SmartPhone();
			sm.setVersion(11.0);
		assertFalse("Invalid version",11.0==sm.getVersion() );
	}
	
	@Test
	public void testSetVersionBoundaryIn() {
		SmartPhone sm = new SmartPhone();
		try {
			sm.setVersion(4.0);
		} catch (VersionNumberException e) {
			System.out.println(e.getMessage());
		}
		assertTrue("Invalid version",4.0==sm.getVersion() );
	}
	@Test(expected = VersionNumberException.class)
	public void testSetVersionBoundaryOut() throws VersionNumberException {
		SmartPhone sm = new SmartPhone();			
		sm.setVersion(4.1);		
		assertFalse("Invalid version",4.1==sm.getVersion() );
	}

}
