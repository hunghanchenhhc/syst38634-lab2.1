package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMillseconds = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue("Invlid number of milliseconds", totalMillseconds == 5);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMillseconds = Time.getTotalMilliseconds("12:05:05:0A");
		fail("Invlid number of milliseconds");
	}

	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMillseconds = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("Invlid number of milliseconds", totalMillseconds == 999);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMillseconds = Time.getTotalMilliseconds("12:05:05:1000");
		fail("Invlid number of milliseconds");
	}
//	@Test
//	public void testGetTotalSecondsRegular() {
//		int totalSeconds = Time.getTotalSeconds("01:01:01");
//		assertTrue("The time provide not match result", totalSeconds == 3661);
//	}
//	
//	@Test( expected = NumberFormatException.class)
//	public void testGetTotalSecondsException() {
//		int totalSeconds = Time.getTotalSeconds("59:59:5a");
//		assertFalse("The time provide not match result", totalSeconds == 362439);
//	}
//	
//	@Test
//	public void testGetTotalSecondsBoundaryIn() {
//		int totalSeconds = Time.getTotalSeconds("01:01:59");
//		assertTrue("The time provide not match result", totalSeconds == 3719);
//	}
//	
//	@Test( expected = NumberFormatException.class)
//	public void testGetTotalSecondsBoundaryOut() {
//		int totalSeconds = Time.getTotalSeconds("01:01:60");
//		assertTrue("The time provide not match result", totalSeconds == 3720);
//	}
}
